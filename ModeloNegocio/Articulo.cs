﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModeloNegocio
{
    public class Articulo : IEquatable<Articulo>
    {
        public string Codigo { get; }
        public string Descripcion { get; set; }

        public string Nombre { get; }

        public double PrecioCoste { get; }

        public override string ToString()
        {
            return ("Codigo: " + this.Codigo + "\r\n" + "Descripción: " + this.Descripcion + "\r\n" + "Nombre: " + this.Nombre);
        }
        public bool Equals(Articulo art)
        {
            return this.Codigo == art.Codigo;
        }
    }
}
